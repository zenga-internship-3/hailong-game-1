﻿using UnityEngine;

public class Lesson2 : MonoBehaviour
{
    [SerializeField] Transform m_Quad;

    Vector3 m_HitPoint;

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (m_HitPoint != Vector3.zero)
                {
                    var angle = Vector3.SignedAngle(m_HitPoint - m_Quad.position, hit.point - m_Quad.position, m_Quad.forward);
                    m_Quad.Rotate(m_Quad.forward, angle);
                }

                m_HitPoint = hit.point;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            m_HitPoint = Vector3.zero;
        }
    }
}
