using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCamera2 : MonoBehaviour
{
    [SerializeField] float m_MinAngle = -10;
    [SerializeField] float m_MaxAngle = 10;

    Vector3 m_PrevMousePosition;
    Transform m_Camera;
    Camera m_CameraComp;

    private void Start()
    {
        m_CameraComp = Camera.main;
        m_Camera = m_CameraComp.transform;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //get mouse position when button down
            m_PrevMousePosition = Input.mousePosition;
        }
        else if (Input.GetMouseButton(0))
        {
            var p1 = m_CameraComp.ScreenToWorldPoint(new Vector3(m_PrevMousePosition.x, m_PrevMousePosition.y, m_CameraComp.nearClipPlane));
            var p2 = m_CameraComp.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, m_CameraComp.nearClipPlane));

            p1.y = m_Camera.position.y;
            p2.y = m_Camera.position.y;

            p1 -= m_Camera.position;
            p2 -= m_Camera.position;

            var angle = Vector3.SignedAngle(p1, p2, Vector3.up);

            m_Camera.Rotate(0, -angle, 0, Space.World);

            //range: ClampAngle
            m_Camera.localEulerAngles = new Vector3(m_Camera.localEulerAngles.x, ClampAngle(m_Camera.localEulerAngles.y, m_MinAngle, m_MaxAngle), m_Camera.localEulerAngles.z);

            m_PrevMousePosition = Input.mousePosition;
        }
    }

    private float ClampAngle(float angle, float min, float max)
    {
        if (angle < 90 || angle > 270)
        {
            if (angle > 180) angle -= 360;
            if (max > 180) max -= 360;
            if (min > 180) min -= 360;
        }
        angle = Mathf.Clamp(angle, min, max);
        if (angle < 0) angle += 360;
        return angle;
    }
}
