using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCamera : MonoBehaviour
{
    private Camera cam;

    private float yaw;

    private int leftRange = 315;
    private int rightRange = 45;

    private Vector3 mousePosDown;
    private Vector3 currentMousePos;
    private Vector3 pointFrom;
    private Vector3 pointTo;

    private Vector3 vectorFrom;
    private Vector3 vectorTo;

    private void Start()
    {
        cam = Camera.main;
    }

    private void Update()
    {
        //mouse position + screen to world point
        if (Input.GetMouseButtonDown(0))
        {
            mousePosDown = Input.mousePosition;
            //pointFrom = cam.ScreenToWorldPoint(new Vector3(mousePosDown.x, mousePosDown.y, cam.nearClipPlane));
        }
        else if (Input.GetMouseButton(0))
        {
            pointFrom = cam.ScreenToWorldPoint(new Vector3(mousePosDown.x, mousePosDown.y, cam.nearClipPlane));
            currentMousePos = Input.mousePosition;
            pointTo = cam.ScreenToWorldPoint(new Vector3(currentMousePos.x, currentMousePos.y, cam.nearClipPlane));

            //get the angle for rotation
            pointFrom.y = Camera.main.transform.position.y;
            pointTo.y = Camera.main.transform.position.y;

            //pointFrom -= cam.transform.position;
            //pointTo -= cam.transform.position;

            vectorFrom = pointFrom - cam.transform.position;
            vectorTo = pointTo - cam.transform.position;

            yaw = Vector3.SignedAngle(vectorFrom, vectorTo, Vector3.up);

            //rotate range

            //cach 1
            cam.transform.localEulerAngles = new Vector3(cam.transform.localEulerAngles.x, ClampAngle(cam.transform.localEulerAngles.y, leftRange, rightRange), cam.transform.localEulerAngles.z);

            ////cach2
            //if (transform.localRotation.eulerAngles.y > rightRange && transform.localRotation.eulerAngles.y < leftRange - 5)
            //    transform.localEulerAngles = new Vector3(0, rightRange, 0);

            //if (transform.localRotation.eulerAngles.y < leftRange && transform.localRotation.eulerAngles.y > rightRange + 5)
            //    transform.localEulerAngles = new Vector3(0, leftRange, 0);

            //rotate camera
            transform.Rotate(0, -yaw, 0, Space.World);

            mousePosDown = Input.mousePosition;
        }
    }

    private float ClampAngle(float angle, float min, float max)
    {
        if (angle < 90 || angle > 270)
        {
            if (angle > 180) angle -= 360;
            if (max > 180) max -= 360;
            if (min > 180) min -= 360;
        }
        angle = Mathf.Clamp(angle, min, max);
        if (angle < 0) angle += 360;
        return angle;
    }

}
