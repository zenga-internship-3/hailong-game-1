using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube1 : MonoBehaviour
{
    public static Cube1 Instance { get; private set; }

    private float moveSpeed = 3.0f;
    Rigidbody rb;

    public bool moveToCube2 = false;
    public bool moveToCube3 = false;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (moveToCube3)
            rb.velocity = new Vector3(moveSpeed, 0, 0);
        else
        {
            if (moveToCube2)
                rb.velocity = new Vector3(-moveSpeed, 0, 0);
            else
                rb.velocity = new Vector3(0, 0, 0);
        }
    }
}
