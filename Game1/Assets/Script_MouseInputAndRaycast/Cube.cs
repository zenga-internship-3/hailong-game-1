using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    private Vector3 clickPos;
    private float cubeYPosition = 0.4999994f;

    private Vector3 GetMouseWorldPos()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            clickPos = hit.point;
        }
        clickPos.y = cubeYPosition;
        return clickPos;
    }

    private void OnMouseDrag()
    {
        transform.position = GetMouseWorldPos();
    }
}

