using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnButton : MonoBehaviour
{
    public GameObject prefab;

    private void Start()
    {
        var createdButton = Instantiate(prefab, transform);
        createdButton.transform.parent = this.transform;
    }
}
