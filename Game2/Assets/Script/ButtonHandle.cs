using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonHandle : MonoBehaviour
{
    string text;
    AudioSource audio;
    bool isClick;
    Button button;

    private void Start()
    {
        audio = GetComponent<AudioSource>();
        Button btn = GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }

    public void TaskOnClick()
    {
        //play audio
        audio.Play();

        //get tmp text from object
        text = this.GetComponentInChildren<TMPro.TMP_Text>().text;

        //debug text
        Debug.Log("Level " + text);
    }
}
